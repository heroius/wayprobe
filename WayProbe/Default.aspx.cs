﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WayProbe
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["func"] != null)
            {
                Response.Clear();
                switch (Request.QueryString["func"])
                {
                    default: // only 'view' for now
                        if (Request.QueryString["rec"] == null)
                        {
                            Response.Write("<p>Bad request!</p>");
                        }
                        else
                        {
                            var path = $"{Server.MapPath("~")}/history/{Request.QueryString["rec"]}.rec";
                            if (File.Exists(path))
                            {
                                using (StreamReader sr = new StreamReader(path, Encoding.UTF8))
                                {
                                    while (!sr.EndOfStream)
                                    {
                                        Response.Write($"<p>{sr.ReadLine()}</p>");
                                    }
                                    sr.Close();
                                }
                            }
                            else
                            {
                                Response.Write("<p>No records.</p>");
                            }
                        }
                        break;
                }
                Response.End();
            }
            else
            {
                Files = new DirectoryInfo($"{Server.MapPath("~")}/history").GetFiles("*.rec", SearchOption.TopDirectoryOnly).Select(file => file.Name.Substring(0, file.Name.Length - 4));
            }
        }

        public IEnumerable<string> Files { get; set; }
    }
}