﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace WayProbe
{
    /// <summary>
    /// 此处理函数用于接受响应
    /// </summary>
    public class PulseHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.QueryString["sender"] != null)
            {
                var root = context.Server.MapPath("~");
                var path = $"{root}/history/{context.Request.QueryString["sender"]}.rec";
                StreamWriter sw;
                if (!File.Exists(path))
                {
                    sw = new StreamWriter(File.Create(path), Encoding.UTF8);
                }
                else
                {
                    sw = new StreamWriter(File.Open(path, FileMode.Append), Encoding.UTF8);
                }
                sw.WriteLine($"{DateTime.Now.ToString("yyyyMMddHHmmss")}:{context.Request.UserHostAddress}");
                sw.Close();
                context.Response.ContentType = "text/plain";
                context.Response.Write("10-4");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}