﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WayProbe.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script type="text/javascript" src="Scripts/jquery-1.8.2.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%if (Files.Count() > 0)
                { %>
            <table>
                <%foreach (var item in Files)
                    {%>
                <tr>
                    <td><%=item %></td>
                    <td>
                        <input type="button" tag="<%=item %>" value="view" onclick="viewInfo(this)" />
                    </td>
                </tr>
                <%} %>
            </table>
            <%}
                else
                { %>
            <p>No history.</p>
            <%} %>
        </div>
        <div id="viewFrame" style="visibility:hidden;">
            <input type="button" value="Close Veiw Port" onclick="hideInfo()" />
            <div id="viewPort" style="overflow-y:scroll;height:200px;"></div>
        </div>
    </form>
    <script type="text/javascript">
        function viewInfo(sender) {
            var button = $(sender);
            var tag = button.attr("tag");
            $.get("default.aspx?func=view", { rec: tag }, function (data, state) {
                $("#viewPort").html(data);
                $("#viewFrame").attr("visibility", "visible");
            }, "html");
        }
        function hideInfo() {
            $("#viewFrame").attr("visibility", "hidden");
        }
    </script>
</body>
</html>
